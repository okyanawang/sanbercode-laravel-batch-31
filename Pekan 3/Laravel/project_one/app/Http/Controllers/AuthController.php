<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }
    public function send(Request $request){
        // dd($request->all());
            $f_name = $request["firstname"];
            $l_name = $request["lastname"];

            return view('home', compact('f_name', 'l_name'));
    }
}
