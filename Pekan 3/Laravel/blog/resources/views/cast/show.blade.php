@extends('layouts.master')

@section('title')
    Cast Detail {{$cast->name}}
@endsection

@section('content')
    <h1>{{$cast->name}}</h1>
    <p>{{$cast->bio}}</p>
@endsection