@extends('layouts.master')

@section('title')
    Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success btn-sm my-2">Add</a>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->age}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Update</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection