<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|unique:cast,name|max:255',
            'age' => 'required',
            'bio' => 'required',
        ],
        [
            'name.required' => 'Name must be filled!',
            'age.required' => 'Age must be filled!',
            'bio.required' => 'Describe the actor/actress!',
        ]);

        DB::table('cast')->insert(
            [
                'name' => $validatedData['name'],
                'age' => $validatedData['age'],
                'bio' => $validatedData['bio']
            ]
        );

        return redirect('/cast');
        // dd($request->all());
    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id){
        $validatedData = $request->validate([
            'name' => 'required|unique:cast,name|max:255',
            'age' => 'required',
            'bio' => 'required',
        ],
        [
            'name.required' => 'Name must be filled!',
            'age.required' => 'Age must be filled!',
            'bio.required' => 'Describe the actor/actress!',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update([
                'name' => $request['name'],
                'age' => $request['age'],
                'bio' => $request['bio']
              ]);
        
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
