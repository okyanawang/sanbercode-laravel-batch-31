<?php

require_once('animal.php');

class Ape extends Animal{
    public $legs = 2;
    public $cold_blooded = "no";
    public $name;

    public function yell(){
        echo "Auooo<br>";
    }
    public function __construct($string){
        $this->name = $string;
    }
}

?>