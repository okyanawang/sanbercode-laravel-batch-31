<?php

require_once('animal.php');

class Frog extends Animal{
    public $legs = 4;
    public $cold_blooded = "no";
    public $name;
    public function jump(){
        echo "hop hop<br>";
    }
    public function __construct($string){
        $this->name = $string;
    }
}

?>