1.
create database myshop;
use myshop;

2.
create table users(
	id integer auto_increment primary key,
	name varchar(255),
	email varchar(255),
	password varchar(255)
);

create table categories(
	id integer auto_increment primary key,
	name varchar(255)
);

create table items(
	id integer auto_increment primary key,
	name varchar(255),
	description varchar(255),
	price integer,
	stock integer,
	category_id integer,
	foreign key(category_id) references categories(id)
);

3.
insert into users(name, email, password)
values ("John Doe", "john@doe.com", "john123");

insert into users(name, email, password)
values ("Jane Doe", "jane@doe.com", "jenita123");

insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

insert into items values(null, "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
	(null, "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
	(null, "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4.
select id, name, email
from users;

select *
from items
where price > 1000000;

select *
from items
where name like "uniklo%";

select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
from items
left join categories on items.category_id = categories.id;

5.
UPDATE items
SET price = 2500000
WHERE name = "Sumsang b50";
